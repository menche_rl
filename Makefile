.POSIX: ;
.SILENT: ;

BINNAME = rl

CC      ?= cc
CFLAGS  += -std=c99 -Wall -Wextra -g3 -ggdb -pipe
LDFLAGS += -lncurses

PREFIX ?= ~
BINDIR  = ${PREFIX}/bin

OBJS = main.o map.o interface.o game.o

default: showconfig ${BINNAME}

all: default

.c.o:
	echo CC $@
	${CC} ${CFLAGS} -c $< -o $@

showconfig:
	echo CC:      ${CC}
	echo CFLAGS:  ${CFLAGS}
	echo LDFLAGS: ${LDFLAGS}

${BINNAME}: ${OBJS}
	echo LD ${BINNAME}
	${CC} ${CFLAGS} ${LDFLAGS} ${OBJS} -o ${BINNAME}

install: ${BINNAME}
	echo installing ${BINNAME}
	install ${BINNAME} ${BINDIR}/

uninstall: 
	echo uninstalling ${BINNAME}
	rm -f ${BINDIR}/${BINNAME}

clean:
	echo cleaning
	rm -f *.o ${BINNAME}

fresh: clean default

.PHONY=showconfig install uninstall clean fresh
