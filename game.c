#include "map.h"
#include "game.h"

enum dir {
	LEFT,
	DOWN,
	UP,
	RIGHT
};

int move_player(struct pos* playerpos, enum tile* map, enum dir direction)
{
	switch(direction) {
	case LEFT:
		if(playerpos->x > 0 &&
			get_tile(map, playerpos->y, playerpos->x - 1) == TILE_FLAT) {
			playerpos->x -= 1;
			return 1;
		}
		break;
	case DOWN:
		if((playerpos->y < 15) &&
			get_tile(map, playerpos->y + 1, playerpos->x) == TILE_FLAT) {
			playerpos->y += 1;
			return 1;
		}
		break;
	case UP:
		if(playerpos->y > 0 &&
			get_tile(map, playerpos->y - 1, playerpos->x) == TILE_FLAT) {
			playerpos->y -= 1;
			return 1;
		}
		break;
	case RIGHT:
		if(playerpos->x < 15 &&
			get_tile(map, playerpos->y, playerpos->x + 1) == TILE_FLAT) {
			playerpos->x += 1;
			return 1;
		}
		break;
	}
	return 0;
}

int step(enum tile* map, struct pos* playerpos, int input)
{
	switch(input) {
	case 'h':
		move_player(playerpos, map, LEFT);
		break;
	case 'j':
		move_player(playerpos, map, DOWN);
		break;
	case 'k':
		move_player(playerpos, map, UP);
		break;
	case 'l':
		move_player(playerpos, map, RIGHT);
		break;
	case 'q':
		return 0;
	default:
		return 1;
	}
	return 1;
}

