#include <stdint.h>
#include <curses.h>
#include "map.h"
#include "interface.h"

void interface_init()
{
	initscr(); cbreak(); noecho(); curs_set(0);
}

void interface_end()
{
	endwin();
}

void interface_draw(enum tile* map, struct pos playerpos)
{
	uint8_t y;
	uint8_t x;
	for(y=0; y<16; y++) {
		for(x=0; x<16; x++) {
			switch(get_tile(map, y, x)) {
			case TILE_FLAT:
				mvaddch(y, x, '.');
				break;
			case TILE_TREE:
				mvaddch(y, x, '#');
				break;
			default:
				mvaddch(y, x, '!');
				break;
			}
		}
		mvaddch(playerpos.y, playerpos.x, '@');
		mvprintw(16, 0, "%2i, %2i", playerpos.y, playerpos.x);
	}
	refresh();
}

