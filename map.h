#define get_tile(sector, y, x) sector[((y) * 16) + (x)]

enum tile {
	TILE_FLAT,
	TILE_TREE
};

struct pos {
	int y;
	int x;
};

enum tile* get_sector(int, int);

